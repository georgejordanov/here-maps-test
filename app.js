const fetch = require('node-fetch');
const randomstring = require("randomstring");
const crypto = require('crypto');

const device = {
    "trackingId": "HERE-96c7fb68-c33f-4bb5-95ae-4d11b1abb61a",
    "deviceId": "9561dc58-3107-4ced-81e5-97124fc98bc4",
    "deviceSecret": "bGe0GQPy5wdMrAWLf4m43KQ0b0QLCXc8gEkGI4l8p1w"
};

const parameters = (ts, rs) => ({
    "realm": "IoT",
    "oauth_consumer_key": device.deviceId,
    "oauth_signature_method": "HMAC-SHA256",
    "oauth_timestamp": ts, 
    "oauth_nonce": rs,
    "oauth_version": "1.0"
})

const tokenURL = "https://tracking.api.here.com/v2/token";


const main = async () => {

    const {timestamp} = await (await fetch('https://tracking.api.here.com/v2/timestamp')).json();
    const randomString = randomstring.generate(6);

    //Generate parameters
    const params = parameters(timestamp, randomString);
    //Sort parameters alphabetically 
    const keys = Object.keys(params).sort((a, b) => a.localeCompare(b));
    // Join all parameters with &
    const combinedParams = keys.map(x => `${x}=${params[x]}`).join("&");
    const urlEncodedParam = encodeURIComponent(combinedParams);
    //Construct base string
    const baseString = `POST&${encodeURIComponent(tokenURL)}&${urlEncodedParam}`;
    
    //Example output: POST&https%3A%2F%2Ftracking.api.here.com%2Fv2%2Ftoken&oauth_consumer_key%3D9561dc58-3107-4ced-81e5-97124fc98bc4%26oauth_nonce%3D3IeJ61%26oauth_signature_method%3DHMAC-SHA256%26oauth_timestamp%3D1541519350%26oauth_version%3D1.0%26realm%3DIoT
    console.log(baseString);

    //Signature - calculate HMAC-SHA256 and to base64
    const signature = crypto.createHmac('SHA256', device.deviceSecret).update(baseString).digest('base64');

    const authorization = `OAuth ${keys.map(x => `${encodeURIComponent(x)}="${encodeURIComponent(params[x])}"`).join(",")}`

    const completeAuth = `${authorization},oauth_signature="${encodeURIComponent(signature)}"`;

    // Example output: OAuth oauth_consumer_key="9561dc58-3107-4ced-81e5-97124fc98bc4",oauth_nonce="3IeJ61",oauth_signature_method="HMAC-SHA256",oauth_timestamp="1541519350",oauth_version="1.0",realm="IoT",oauth_signature="pzmFclGoXf7nBkSxzrsLja8YtyoQFPYkGfGgL6fKSA4%3D"
    console.log(completeAuth);

    const result = await (await fetch(tokenURL, {
    method: 'POST',
    headers: {
      'Authorization': completeAuth,
      'Content-Type': 'application/json'
    }})).json();

    // Example output:
    //  { code: 401,
    //    id: '1a7f4d15-5e5f-4d38-b4f6-08700917ec45',
    //    message: 'Signature mismatch. Authorization signature or thing credential is wrong.',
    //    error: '*Timestamp wrong*\n\nWhen the request timestamp is more than 10 seconds skewed from the\nserver time, the `x-here-timestamp` header with the current server\ntimestamp is added to the response.\n\n*Incorrect Signature*\n\nIf the OAuth signature is incorrect, the response will be a 401 but\n*without* the `x-here-timestamp` field.\n' }
    console.log(result);

}

main();